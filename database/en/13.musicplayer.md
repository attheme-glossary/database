# In-app Music Player

The in-app editor opener button is overlaid when you open the editor. All player
variables are available on the chats list screen, or you can use
[.attheme editor](http://snejugal.ru/attheme-editor) to change the variables.

## Action bar

If you tap the top-left icon, you'll see an old-like player with action bar. It
also appears when the playlist is big and you scroll the list.

<glossary-variable color="red">

### player_actionBar

Set the same things actionBarDefault, actionBarDefaultIcon,
actionBarDefaultTitle, actionBarDefaultSubtitle and actionBarDefaultSelector
respectively do.

</glossary-variable>

<glossary-variable color="purple">

### player_actionBarItems

</glossary-variable>

<glossary-variable color="yellow">

### player_actionBarTitle

</glossary-variable>

<glossary-variable color="blue">

### player_actionBarSubtitle

</glossary-variable>

<glossary-variable color="green">

### player_actionBarSelector

</glossary-variable>

<glossary-variable color="orange">

### player_actionBarTop

Sets the status bar background color on Android 5.0 and higher, and, unlike in
any other case, it's not darkened by 20% (if you want to do it, set the value to
0, 0, 0, 51). Setting the alpha channel of this variable to zero fallbacks to
player_actionBar.

</glossary-variable>

<figure>

![](./images/musicplayer.0.png)

<figcaption>

The red area shows `player_actionBar`, the purple areas show
`player_actionBarItems”`, the yellow area shows `player_actionBarTop`, the blue
circle shows `player_actionBarSelector`, the green underline shows
`player_actionBarTitle`, and the orange underline shows
`player_actionBarSubtitle`.

</figcaption>
</figure>

## Playlist panel

For the blue-to-pink area, see the Action Bar subsection.

For the gray-to-green area, see the Placeholder subsection.

<glossary-variable color="red">

### player_background

Sets the background of the panel and also the seekbar panel.

</glossary-variable>

## Seekbar

<glossary-variable color="blue">

### player_progress

Sets the played part indicator.

</glossary-variable>

<glossary-variable color="brown">

### player_progressBackground

Sets the unplayed part indicator.

</glossary-variable>

<glossary-variable color="lime">

### player_time

Sets the color of the track duration and the played part time.

</glossary-variable>

<glossary-variable color="orange">

### player_button

Sets the color of non-pressed or inactive buttons.

</glossary-variable>

<glossary-variable color="pink">

### player_buttonActive

Sets the color of pressed or active buttons.

</glossary-variable>

## The list

This subsection doesn't describe new variables, these variables aren't used only
here, their primary usage can be found in other sections of the glossary.

<glossary-variable color="purple">

### windowBackgroundWhiteBlackText

Sets the track title color.

</glossary-variable>

<glossary-variable color="indigo">

### windowBackgroundWhiteGrayText2

Sets the color of the performer's name.

</glossary-variable>

<glossary-variable color="green">

### chat_inLoader

Sets the background color of play/pause buttons on the list.

</glossary-variable>

<glossary-variable color="green">

### chat_inBubble

Sets the icon color of play/pause buttons on the list.

</glossary-variable>

<glossary-variable color="gray">

### actionBarDefaultSubmenuBackground

Sets the background of submenus, e. g. when you tap the reverse button. The
icons there are set by `player_button` and `player_buttonActive`.

</glossary-variable>

<figure>

![](./images/musicplayer.1.png)

<figcaption>

The red area shows `player_background`, the blue gray underline shows
`player_progress`, the brown underline shows `player_progressBackground`, the
lime underlines show `player_time`, the orange areas show `player_buttonActive`,
the pink areas show `player_buttonActive`, the purple underlines show
`windowBackgroundWhiteBlackText`, the blue underlines show
`windowBackgroundWhiteGrayText2`, the green circles show `chat_inLoader`. For
gradient-stroked areas see the top of this subsection.

</figcaption>
</figure>

## Placeholder

You see the placeholder when the track doesn't have a cover.

<glossary-variable color="red">

### player_placeholderBackground

Sets the background color of the placeholder.

</glossary-variable>

<glossary-variable color="blue">

### player_placeholder

Sets the color of the placeholder icon.

</glossary-variable>

<figure>

![](./images/musicplayer.2.png)

<figcaption>

The red area shows `player_placeholderBackground` and the blue area shows
`player_placeholder`.

</figcaption>
</figure>

## Top panel

When you start playing a track or a voice message, a panel at the top and below
the action bar with audio controls appears. This subsection is about this panel.

<glossary-variable color="red">

### inappPlayerBackground

Sets the background color of the in-app player panel.

</glossary-variable>

<glossary-variable color="green">

### inappPlayerPlayPause

Sets the color of the play/pause button of the in-app player panel.

</glossary-variable>

<glossary-variable color="blue">

### inappPlayerPerformer

Sets the text color of who performed the audio/song playing on the in-app player
panel.

</glossary-variable>

<glossary-variable color="purple">

### inappPlayerTitle

Sets the text color on the in-app player panel: for songs it sets the color
song's name color, for voice messages shows when it was sent, for example
yesterday at 08:05 A.M.

</glossary-variable>

<glossary-variable color="orange">

### inappPlayerClose

Sets the color of the close button inside the in-app player panel.

</glossary-variable>

<figure>

![](./images/musicplayer.3.png)

<figcaption>

The red area shows `inappPlayerbackground`, the green area shows
`inappPlayerPlayPause`, the orange area shows `inappPlayerClose`, the blue
underline shows `inappPlayerPerformer`, and the purple area shows
`inappPlayerTitle`.

</figcaption>
</figure>

**Note by @Ra1nb0wD4sh:** `inappPlayerClose` affects the color of the 2x
playback toggle (appeared in 4.8.10) while it's in disabled state.
`inappPlayerPlayPause` affects the color it has when enabled.

<figure>

![](./images/musicplayer.4.png)

<figcaption>

The 2x playback toggle

</figcaption>
</figure>
