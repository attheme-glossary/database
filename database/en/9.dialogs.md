# Dialogs

Dialogs are the panels at the bottom or in the middle of the screen. For
example, the one that appears when you tap a message or that one that appears
when you hold a chat on the chat list.

## Backgrounds

<glossary-variable color="red">

### dialogBackground

Sets the background of dialogs.

</glossary-variable>

<glossary-variable color="green">

### dialogBackgroundGray

Sets the small space between sections in Supergroup settings → Event Log →
Settings.

</glossary-variable>

<figure>

![](./images/dialogs.bg.png)

<figcaption>

The red area shows `dialogBackground` and the green area shows
`dialogBackgroundGray`.

</figcaption>
</figure>

## Text

<glossary-variable color="red">

### dialogTextBlack

Sets the color of almost all text on dialogs.

</glossary-variable>

<figure>

![](./images/dialogs.text.0.png)

<figcaption>

The red underlines show `dialogTextBlack`.

</figcaption>
</figure>

<glossary-variable color="red">

### dialogTextGray2

Sets the color of descriptions on the attach panel.

</glossary-variable>

<figure>

![](./images/dialogs.text.1.png)

<figcaption>

The red underlines show `dialogTextGray2`.

</figcaption>
</figure>

### Blue text

<glossary-variable color="red">

#### dialogLinkSelection

Sets the link in sticker pack name underlay when you tap it.

</glossary-variable>

<figure>

![](./images/dialogs.text.2.png)

<figcaption>

The red underlines show `dialogTextBlack`.

</figcaption>
</figure>

## Decorative elements

<glossary-variable color="red">

### dialogIcon

Sets the color of icons. For example, the ones that appear when you hold a chat
on the chat list.

</glossary-variable>

<glossary-variable color="green">

### dialogGrayLine

Sets the color of the border you can see in the in-app video player panel. You
can open it by attaching a YouTube video link to your message, then tap the
video preview image.

</glossary-variable>

<figure>

![](./images/dialogs.text.delem.png)

<figcaption>

The red area shows `dialogIcon` and the green area shows `dialogGrayLine`.

</figcaption>
</figure>

## Badge

<glossary-variable color="red">

### dialogBadgeBackground

Sets the background color of a badge — the counter you see when you forward
messages or add stickers.

</glossary-variable>

<glossary-variable color="green">

### dialogBadgeText

Sets the text color on the badge when forwarding message but not when adding
stickers — it's set by picker_badgeText.

</glossary-variable>

<figure>

![](./images/dialogs.badge.png)

<figcaption>

The red area shows `dialogBadgeBackground` and the green area shows
`dialogBadgeText`.

</figcaption>
</figure>

## Controls

### Buttons

<glossary-variable color="red">

#### dialogButton

Sets the text color of buttons on dialogs on those that appear in the middle of
the screen — for example, when you're deleting a message.

</glossary-variable>

<glossary-variable color="green">

#### dialogButtonSelector

Sets the button background color when you tap it.

</glossary-variable>

<figure>

![](./images/dialogs.btn.png)

<figcaption>

The red underlines show `dialogButton` and the green area shows
`dialogButtonSelector`.

</figcaption>
</figure>

### Radio buttons

They should set radio buttons colors on dialogs, but they don't for some unknown
reasons.

<glossary-variable color="gray">

#### dialogRadioButton

</glossary-variable>

<glossary-variable color="gray">

#### dialogRadioButtonChecked

</glossary-variable>

### Square checkboxes

Set the same elements of the square checkboxes but on dialogs.

<glossary-variable color="indigo">

#### dialogCheckboxSquareBackground

</glossary-variable>

<glossary-variable color="green">

#### dialogCheckboxSquareCheck

</glossary-variable>

<glossary-variable color="orange">

#### dialogCheckboxSquareDisabled

</glossary-variable>

<glossary-variable color="red">

#### dialogCheckboxSquareUnchecked

</glossary-variable>

<figure>

![](./images/dialogs.sCheckbox.png)

<figcaption>

The red area shows `dialogCheckboxSquareUnchecked`, the blue area shows
`dialogCheckboxSquareBackground`, the green area shows
`dialogCheckboxSquareCheck`, and the orange area shows
`dialogCheckboxSquareDisabled` (note that it is how a disabled checkbox may look
like, but we're not sure about that because we don't know where it is used).

</figcaption>
</figure>

### Round checkboxes

<glossary-variable color="red">

#### dialogRoundCheckBox

Sets the circle background that appears when you select chats you want to
forward messages to on the forward panel.

</glossary-variable>

<glossary-variable color="green">

#### dialogRoundCheckBoxCheck

Sets the color of the check mark on that circle.

</glossary-variable>

<figure>

![](./images/dialogs.rCheckbox.png)

<figcaption>

The red area shows `dialogRoundCheckBox` and the green area shows
`dialogRoundCheckBoxCheck`.

</figcaption>
</figure>

## Text fields

<glossary-variable color="red">

### dialogInputField

Sets the text field bottom border when the field is inactive. Inactive state is
seen when you rate a call for 4 or less stars; you can get the ”Rate Call”
prompt when you hold a call in Calls Log.

</glossary-variable>

<glossary-variable color="green">

### dialogInputFieldActivated

Sets the color of the bottom border below text fields when the field is active —
almost all text fields on dialogs right after they appear.

</glossary-variable>

<figure>

![](./images/dialogs.field.png)

<figcaption>

The red area shows `dialogInputField` and the green area shows
`dialogInputFieldActivated`.

</figcaption>
</figure>

## Progress indicators

<glossary-variable color="red">

### dialogLineProgressBackground

Sets the background color of line progress you can see when you save to
downloads any file (three dots beside a file on chat screen → Save to
downloads).

</glossary-variable>

<glossary-variable color="green">

### dialogLineProgress

Sets the color of the line that shows what percent has already been downloaded.

</glossary-variable>

<figure>

![](./images/dialogs.progress.png)

<figcaption>

The red area shows `dialogLineProgressBackground` and the green area shows
`dialogLineProgress`.

</figcaption>
</figure>

## Attach panel

<glossary-variable color="red">

### dialogScrollGlow

Sets the glow color you can see when you quickly scroll the attach panel.

</glossary-variable>

<figure>

![](./images/dialogs.attachpanel.0.png)

<figcaption>

The black glow is `dialogScrollGlow`. In the default theme, it is white, and on
this example image, it was made black, so one can clearly see it.

</figcaption>
</figure>

Next variables set the background color of attach buttons.

<glossary-variable color="gray">

### chat_attachGalleryBackground

</glossary-variable>

<glossary-variable color="gray">

### chat_attachVideoBackground

</glossary-variable>

<glossary-variable color="gray">

### chat_attachMusicBackground

</glossary-variable>

<glossary-variable color="gray">

### chat_attachFileBackground

</glossary-variable>

<glossary-variable color="gray">

### chat_attachContactBackground

</glossary-variable>

<glossary-variable color="gray">

### chat_attachLocationBackground

</glossary-variable>

<glossary-variable color="gray">

### chat_attachHideBackground

</glossary-variable>

<glossary-variable color="gray">

### chat_attachSendBackground

</glossary-variable>

Next variables set the icon color of attach buttons.

<glossary-variable color="gray">

### chat_attachGalleryIcon

</glossary-variable>

<glossary-variable color="gray">

### chat_attachVideoIcon

</glossary-variable>

<glossary-variable color="gray">

### chat_attachMusicIcon

</glossary-variable>

<glossary-variable color="gray">

### chat_attachFileIcon

</glossary-variable>

<glossary-variable color="gray">

### chat_attachContactIcon

</glossary-variable>

<glossary-variable color="gray">

### chat_attachLocationIcon

</glossary-variable>

<glossary-variable color="gray">

### chat_attachHideIcon

</glossary-variable>

<glossary-variable color="gray">

### chat_attachSendIcon

</glossary-variable>

<figure>

![](./images/dialogs.attachpanel.1.png)

<figcaption>

The buttons' backgrounds are changed by `chat_attach*Background` and their icons
are changed by `chat_attach*Icon`.

</figcaption>
</figure>

<figure>

![](./images/dialogs.attachpanel.2.png)

<figcaption>

This button is `chat_attachSendBackground` (it shows when you select at least
one photo to send).

</figcaption>
</figure>

Next variables set the colors of the “leaves” of the camera icon, in the
clockwise direction.

<glossary-variable color="gray">

### chat_attachCameraIcon1

</glossary-variable>

<glossary-variable color="gray">

### chat_attachCameraIcon2

</glossary-variable>

<glossary-variable color="gray">

### chat_attachCameraIcon3

</glossary-variable>

<glossary-variable color="gray">

### chat_attachCameraIcon4

</glossary-variable>

<glossary-variable color="gray">

### chat_attachCameraIcon5

</glossary-variable>

<glossary-variable color="gray">

### chat_attachCameraIcon6

</glossary-variable>

<figure>

![](./images/dialogs.attachpanel.3.png)

<figcaption>

The numbers show where each “leaf” of the icon is.

</figcaption>
</figure>

- The color of descriptions below buttons is `dialogTextGray2`.
