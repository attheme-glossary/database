# Терминология

В этом разделе содержится список терминов, с которыми рекомендуется
ознакомиться, прежде чем начать пользоваться словарём.

## Резервная переменная

**Резервная переменная** &mdash; это переменная, которая используется вместо
описываемой при выполнении следующих условий:

- описываемая переменная отсутствует в файле темы, т.е. для неё установлен цвет
  по умолчанию;
- резервная переменная присутствует в файле темы, т.е. для неё установлен
  пользовательский цвет.

Например, для [`dialogShadowLine`] резервной переменной является
`chat_emojiPanelShadowLine`. Возможны следующие ситуации:

- обе переменные отсутствуют в файле темы — для обеих переменных используются
  цвета из стандартной темы;
- `dialogShadowLine` отсутствует в файле темы, а `chat_emojiPanelShadowLine` —
  присутствует. В этом случае первая переменная окрашивается в цвет последней;
- `dialogShadowLine` присутствует в файле темы — цвет обеих переменных
  определяется как обычно.

[`dialogshadowline`]: #диалоговые-окна/разделители/dialogshadowline
