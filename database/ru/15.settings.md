# Настройки

## Иконки

<glossary-variable color="green">

### sessions_devicesImage

Задаёт цвет иконки паспорта в Настройках → Конфиденциальность → Telegram
Passport. Этот раздел можно увидеть, если Вы ранее пользовались [Telegram
Passport], но на данный момент данные отсутствуют.

</glossary-variable>

<glossary-variable color="pink">

### windowBackgroundWhiteGrayText7

Задаёт цвет иконки у тем, которые не поддерживаются Telegram для Android либо
ещё не загрузились в Настройках → Настройки чатов → Настройки темы.

**Примечание:** если Вы используете редактор тем приложения, эту переменную
можно изменить, перейдя в настройки кода-пароля.

</glossary-variable>

<figure>

![](./images/settings.0.placeholder.png)

<figcaption>

Зелёное — `sessions_devicesImage`,  
розовое — `windowBackgroundWhiteGrayText7`.

</figcaption>
</figure>

## Стикеры

Можно найти в Настройках → Стикеры и эмодзи.

<glossary-variable color="indigo">

### stickers_menu

Задаёт цвет иконки меню справа у наборов стикеров.

</glossary-variable>

<glossary-variable color="red">

### stickers_menuSelector

Задаёт цвет эффекта нажатия на иконку меню справа у наборов стикеров.

</glossary-variable>

<figure>

![](./images/settings.1.stickersMenu.png)

<figcaption>

Тёмно-синее — `stickers_menu`,  
красное — `stickers_menuSelector`.

</figcaption>
</figure>

<glossary-variable color="green">

### featuredStickers_addButton

Задаёт цвет фона кнопки "Добавить" в архиве стикеров.

**Будьте внимательны:** эта переменная также задаёт цвет текста кнопки "Оставить
_<текущий номер телефона>_" на экране смены номера телефона.

</glossary-variable>

<glossary-variable color="yellow">

### featuredStickers_buttonText

Задаёт цвет текста кнопки "Добавить" в архиве стикеров.

</glossary-variable>

<glossary-variable color="orange">

### featuredStickers_addButtonPressed

Задаёт цвет эффекта нажатия на кнопку "Добавить" в архиве стикеров.

</glossary-variable>

<glossary-variable color="purple">

### featuredStickers_buttonProgress

Задаёт цвет индикатора загрузки при нажатии на кнопку "Добавить" в архиве
стикеров.

**Примечание:** если Вы используете редактор тем приложения, эту переменную
можно изменить, открыв и закрыв окно популярных стикеров в Настройках.

</glossary-variable>

<glossary-variable color="red">

### featuredStickers_removeButtonText

Задаёт цвет текста кнопки "Удалить" в архиве стикеров.

[**Резервная переменная:**] `featuredStickers_addButtonPressed`.

</glossary-variable>

<glossary-variable color="indigo">

### featuredStickers_unread

Задаёт цвет индикатора у непросмотренных наборов в окне популярных стикеров.

**Примечание:** если Вы используете редактор тем приложения, эту переменную
можно изменить, открыв и закрыв окно популярных стикеров в Настройках.

</glossary-variable>

<figure>

![](./images/settings.2.stickersBtn.png)

<figcaption>

Зелёное — `featuredStickers_addButton`,  
жёлтое — `featuredStickers_buttonText`,  
оранжевое — `featuredStickers_addButtonPressed`,  
фиолетовое — `featuredStickers_buttonProgress`,  
красное — `featuredStickers_removeButtonText`,  
тёмно-синее — `featuredStickers_unread`.

</figcaption>
</figure>

[**резервная переменная:**]: #терминология/резервная-переменная
[telegram passport]: #справочник/telegram-passport
